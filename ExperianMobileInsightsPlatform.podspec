Pod::Spec.new do |s|

  s.name         = "ExperianMobileInsightsPlatform"
  s.version      = "2.0.1"
  s.summary      = "ExperianMobileInsightsPlatform is a user metadata collector framework to score user's banking activities."

  s.description  = <<-DESC
  ExperianMobileInsightsPlatform is a user metadata collector framework to score user's banking activities
  destinated to Serasa Experian Partners.
  DESC

  s.homepage     = "https://code.experian.local/projects/mobileip/repos/ios-experian-mobile-insights-platform/"

  s.license      = { :type => "MIT" }

  s.author             = { "Andrey Souza" => "andrey.souza@br.experian.com" }

  s.platform = :ios
  s.swift_version = '5.3'
  s.ios.deployment_target = '10.0'

  s.source       = { :git => "https://bitbucket.org/datalabbr/serasaexperian-datalab-mip-sdk-ios.git", :tag => s.version.to_s }
  s.vendored_frameworks  = 'ExperianMobileInsightsPlatform.xcframework'

end