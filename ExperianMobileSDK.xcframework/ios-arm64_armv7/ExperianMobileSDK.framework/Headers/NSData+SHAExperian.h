//
//  NSData_SHA1.h
//  SwiftyRSA
//
//  Created by Paul Wilkinson on 19/04/2016.
//  Copyright © 2016 Scoop. All rights reserved.
//

#import "Foundation/Foundation.h"
#import "ExperianMobileSDK/ExperianMobileSDK.h"

@interface NSData (NSData_SwiftyRSASHAExperian)

- (nonnull NSData*) SwiftyRSASHA1Experian;
- (nonnull NSData*) SwiftyRSASHA224Experian;
- (nonnull NSData*) SwiftyRSASHA256Experian;
- (nonnull NSData*) SwiftyRSASHA384Experian;
- (nonnull NSData*) SwiftyRSASHA512Experian;

@end
