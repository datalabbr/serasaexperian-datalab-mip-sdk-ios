// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "ExperianMobileInsightsPlatform",
	products: [
		.library(
			name: "ExperianMobileInsightsPlatform",
			targets: ["ExperianMobileInsightsPlatform"]),
	],
	dependencies: [],
	targets: [
		.binaryTarget(name: "ExperianMobileInsightsPlatform", path: "ExperianMobileInsightsPlatform.xcframework")
	]
)
