Pod::Spec.new do |s|

  s.name         = "ExperianMobileSDK"
  s.version      = "1.3.9"
  s.summary      = "ExperianMobileSDK is a user metadata collector framework to score user's banking activities."

  s.description  = <<-DESC
  ExperianMobileSDK is a user metadata collector framework to score user's banking activities
  destinated to Serasa Experian Partners.
  DESC

  s.homepage     = "https://bitbucket.org/mip-latam/ios-mobile-sdk"

  s.license      = { :type => "MIT" }

  s.author             = { "Andrey Souza" => "andrey.souza@br.experian.com" }

  s.platform = :ios
  s.swift_version = '5.3'
  s.ios.deployment_target = '10.0'

  s.source       = { :git => "https://bitbucket.org/datalabbr/serasaexperian-datalab-mip-sdk-ios.git", :tag => s.version.to_s }
  s.vendored_frameworks  = 'ExperianMobileSDK.xcframework'

end