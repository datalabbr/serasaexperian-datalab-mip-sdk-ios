# ExperianMobileInsightsPlatform

### Manual de Instalação e Utilização

#### Versão 2.0

## Pré requisitos

- Xcode 12
- Swift 5+ ou Objective-C
- iOS 11.4+

## Instalação via Cocoapods
Em seu **Podfile** insira a seguinte linha:

```ruby
platform :ios, '11.4'

target 'MeuProjeto' do
  use_frameworks!

  # Pods for MeuProjeto
  pod 'ExperianMobileInsightsPlatform', :source => "https://bitbucket.org/datalabbr/serasaexperian-datalab-mip-sdk-ios.git"

end
```

## Instalação via Swift Package Manager
Adicione um novo **Package Dependency** como na imagem/
![](./assets/img_spm_01.png)

Na área de pesquisa procure pelo seguinte endereço: https://bitbucket.org/datalabbr/serasaexperian-datalab-mip-sdk-ios.git

![](./assets/img_spm_02.png)

Selecione o **Target** do seu projeto

![](./assets/img_spm_03.png)

Aparecerá uma tela para confirmar os dados da nova dependencia
![](./assets/img_spm_04.png)

E uma tela para confirmar o **Target** em que será adicionado o **Package**
![](./assets/img_spm_05.png)

## Execução

### Swift

Implemente o protocolo **ClientIdentifiable** no objeto de sua preferencia.

```swift
import ExperianMobileInsightsPlatform
 
 ...

class Client: ClientIdentifiable {
	var partnerCode: String = "PARTNERCODE"
	var clientCode: String = "Teste123"
	var privacyPolicy: String = "1.0"
	var acceptedAgreements: [Int] = [ExperianAgreementStatusEnum.creditAgreementStatus.rawValue]
}
```

Agora basta chamar a função estática **collect** passando como parametro o objeto que implementa **ClientIdentifiable**.
```swift
import ExperianMobileInsightsPlatform
 
 ...

 ExperianMIP.collect(client: Client())

 ...

```

### Observação
Alguns tipos de coletas necessitam autorização do usuário, caso seja necessário, o **App** deve fazer essa requisição. Para ajudar, seguem alguns exemplos de implementação e as linhas que devem ser adicionadas ao **Info.plist**.

```swift
struct AuthorizationRequester {
	static func requestPhotoAuthorization() {
		PHPhotoLibrary.requestAuthorization {_ in}
	}
	static func requestContactsAuthorization() {
		CNContactStore().requestAccess(for: .contacts) {_,_ in}
	}
	static func requestCalendarAuthorization() {
		EKEventStore().requestAccess(to: .event) {_,_ in}
	}
	static func requestAllAuthorizations() {
		requestPhotoAuthorization()
		requestContactsAuthorization()
		requestCalendarAuthorization()
	}
}

...

class ViewController: UIViewController {
	let clLocationManager = CLLocationManager()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		clLocationManager.requestAlwaysAuthorization()
		AuthorizationRequester.requestAllAuthorizations()
		ExperianMIP.collect(client: Client())
	}
}
```

#### Info.plist
```plist
	<key>NSBluetoothAlwaysUsageDescription</key>
		<string>For better data analysis we need access to bluetooth</string>
	<key>NSBluetoothPeripheralUsageDescription</key>
		<string>For better data analysis we need access to bluetooth</string>
	<key>NSCalendarsUsageDescription</key>
		<string>For better data analysis we need access to calendar</string>
	<key>NSContactsUsageDescription</key>
		<string>For better data analysis we need access to contacts</string>
	<key>NSLocalNetworkUsageDescription</key>
		<string>For better data analysis we need access to network</string>
	<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
		<string>For better data analysis we need access to location</string>
	<key>NSLocationAlwaysUsageDescription</key>
		<string>For better data analysis we need access to location</string>
	<key>NSLocationWhenInUseUsageDescription</key>
		<string>For better data analysis we need access to location</string>
	<key>NSPhotoLibraryUsageDescription</key>
		<string>For better data analysis we need access to media</string>
	<array>
		<string>bbapp</string>
		<string>BDNiPhoneVarejo</string>
		<string>BDNiPhonePrime</string>
		<string>nuapp</string>
		<string>itauvarejo</string>
		<string>itaupersonnalite</string>
		<string>sicoob</string>
		<string>banconeon</string>
		<string>rastreiocorreios</string>
		<string>ifood</string>
		<string>santanderpf</string>
		<string>uber</string>
		<string>whatsapp</string>
		<string>fb</string>
		<string>instagram</string>
		<string>twitter</string>
		<string>picpay</string>
	</array>
	<?xml version="1.0" encoding="UTF-8"?>
```

### Build em Objective C

Dentro da Aba **Build Settings** do target desejado, atribua o valor **Yes** para o atributo **Always Embed Swift Standard Libraries**. Então implemente o protocolo **ClientIdentifiable** no objeto de sua preferencia.

#### Client.h
```objc
#import <Foundation/Foundation.h>
#import "ExperianMobileInsightsPlatform/ExperianMobileInsightsPlatform-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface Client : NSObject <ClientIdentifiable>

@property (nonatomic, readonly, copy) NSString * _Nonnull partnerCode;
@property (nonatomic, readonly, copy) NSString * _Nonnull clientCode;
@property (nonatomic, readonly, copy) NSString * _Nonnull privacyPolicy;
@property (nonatomic, readonly, copy) NSArray<NSNumber *> * _Nonnull acceptedAgreements;

- (instancetype)initWithPartnerCode: (NSString *) partnerCode clientCode: (NSString *) clientCode privacyPolicy: (NSString *) privacyPolicy acceptedAgreements: (NSArray<NSNumber *> *) acceptedAgreements;

@end

NS_ASSUME_NONNULL_END

```
#### Client.m
```objc
#import "Client.h"
#import "ExperianMobileInsightsPlatform/ExperianMobileInsightsPlatform-Swift.h"

@implementation Client

- (instancetype)initWithPartnerCode: (NSString *) partnerCode clientCode: (NSString *) clientCode privacyPolicy: (NSString *) privacyPolicy acceptedAgreements: (NSArray<NSNumber *> *) acceptedAgreements {
	self = [super init];
	if (self) {
		_partnerCode = partnerCode;
		_clientCode = clientCode;
		_privacyPolicy = privacyPolicy;
		_acceptedAgreements = acceptedAgreements;
	}
	return self;
}

@end

```

Agora basta chamar a função estática **collect** passando como parametro o objeto que implementa **ClientIdentifiable**.

```objc
#import "Client.h"
#import "AuthorizationRequester.h"
#import "ExperianMobileInsightsPlatform/ExperianMobileInsightsPlatform-Swift.h"
#import <CoreLocation/CoreLocation.h>
 
 @implementation MyClass

 ...

 CLLocationManager *locationManager;

 ...

-(void)callExperianSDK {
	locationManager = [[CLLocationManager alloc] init];
			[locationManager requestAlwaysAuthorization];

			Client *client = [[Client alloc]initWithPartnerCode:@"PARTNERCODE" clientCode:@"Teste123" privacyPolicy:@"1.0" acceptedAgreements:@[@2]];
			[AuthorizationRequester requestAllAuthorizations];
			[ExperianMIP collectWithClient: client completionHandler: NULL];
}

 ...

 @end

```

### Observação
Alguns tipos de coletas necessitam autorização do usuário, caso seja necessário, o **App** deve fazer essa requisição. Para ajudar, seguem alguns exemplos de implementação e as linhas que devem ser adicionadas ao **Info.plist**.

#### AuthorizationRequester.h
```objc
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AuthorizationRequester : NSObject

+(void) requestPhotoAuthorization;
+(void) requestContactsAuthorization;
+(void) requestCalendarAuthorization;
+(void) requestAllAuthorizations;

@end

NS_ASSUME_NONNULL_END


...

class ViewController: UIViewController {
	let clLocationManager = CLLocationManager()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		clLocationManager.requestAlwaysAuthorization()
		AuthorizationRequester.requestAllAuthorizations()
		ExperianMIP.collect(client: Client())
	}
}
```

#### AuthorizationRequester.m
```objc
#import "AuthorizationRequester.h"
#import <Photos/Photos.h>
#import <Contacts/Contacts.h>
#import <EventKit/EventKit.h>

@implementation AuthorizationRequester

+(void)requestPhotoAuthorization {
	[PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
	}];
}

+(void)requestContactsAuthorization {
	[[[CNContactStore alloc] init] requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {}];
}

+(void)requestCalendarAuthorization {
	[[[EKEventStore alloc] init] requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error) {}];
}

+(void)requestAllAuthorizations {
	[self requestPhotoAuthorization];
	[self requestContactsAuthorization];
	[self requestCalendarAuthorization];
}

@end
```

#### Info.plist
```plist
	<key>NSBluetoothAlwaysUsageDescription</key>
		<string>For better data analysis we need access to bluetooth</string>
	<key>NSBluetoothPeripheralUsageDescription</key>
		<string>For better data analysis we need access to bluetooth</string>
	<key>NSCalendarsUsageDescription</key>
		<string>For better data analysis we need access to calendar</string>
	<key>NSContactsUsageDescription</key>
		<string>For better data analysis we need access to contacts</string>
	<key>NSLocalNetworkUsageDescription</key>
		<string>For better data analysis we need access to network</string>
	<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
		<string>For better data analysis we need access to location</string>
	<key>NSLocationAlwaysUsageDescription</key>
		<string>For better data analysis we need access to location</string>
	<key>NSLocationWhenInUseUsageDescription</key>
		<string>For better data analysis we need access to location</string>
	<key>NSPhotoLibraryUsageDescription</key>
		<string>For better data analysis we need access to media</string>
	<array>
		<string>bbapp</string>
		<string>BDNiPhoneVarejo</string>
		<string>BDNiPhonePrime</string>
		<string>nuapp</string>
		<string>itauvarejo</string>
		<string>itaupersonnalite</string>
		<string>sicoob</string>
		<string>banconeon</string>
		<string>rastreiocorreios</string>
		<string>ifood</string>
		<string>santanderpf</string>
		<string>uber</string>
		<string>whatsapp</string>
		<string>fb</string>
		<string>instagram</string>
		<string>twitter</string>
		<string>picpay</string>
	</array>
	<?xml version="1.0" encoding="UTF-8"?>
```